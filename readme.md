# Urban Geenery Project
#### Public Website for the Urban Greenery Project
https://hayatotakai.gitlab.io/urban-forestry-website

## Overview
New research has shown that temperatures on a scorching summer day can vary as much as 20 degrees across different parts of the same city, with marginalized neighborhoods often bearing the brunt of that heat. The goal of this project is to develop the needed capabilities to address this problem from a scientific perspective. The team will use funding from a recent grant to grow a small Miyawaki forest in the Bronx borough of New York City with the help of local communities, which will be the focus of a long-term urban microclimate monitoring campaign.

Fluid mechanics simulations were conducted and satellite temperature data were analyzed to plan the optimal location and layout for the forest. More information of each component can be found in their respective folders. 



## Goals
- [x] Survey target location
- [ ] Run fluid mechanics simulations for canopy flow
- [x] Obtain satellite temperature data for target location
- [x] Create public website
- [ ] Secure location 
- [ ] Search for volunteers from the local community
- [ ] Build forest