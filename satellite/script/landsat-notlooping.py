#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 29 16:44:53 2021
Adapted on Thu Jun 28 24:09:09 2022
@author: meeramavroidis, adapted by hayatotakai
"""

import json
from tkinter.tix import Y_REGION
from landsatxplore.api import API
from landsatxplore.earthexplorer import EarthExplorer
import rasterio
from rasterio import plot
import numpy as np
import matplotlib.pyplot as plt
# %matplotlib inline
import matplotlib.colors as mcol
import matplotlib.cm as cm
import os
import tarfile
from colorama import Fore, Back, Style
from pwinput import pwinput
from matplotlib.colors import LinearSegmentedColormap

#####################


def urban_greenery_satellite(dataset, latitude, longitude, start_date, end_date, xPosition, yPosition, size):
    '''
    Landsat8_to_LSTregionsmap: searches for and downloads a single user-selected Landsat 8 data scene from the USGS archive (user must have a UGSG account), it then converts this data into a Land Surface Temperature (LST) map of New York City
        note: the user enters the search parameters and a variety of options will be presented, the user is then instructed to type in the Scene Identifier of a single scene they wish to download, and that scene will be downloaded and converted into an LST map

    Arguments:
        dataset --> the specific Landsat 8 dataset format in string form (e.g. Landsat 8 Level 1 Collection 1, Landsat 8 Level 1 Collection 2, Landsat 8 Level 2 Collection 1, Landsat 8 Level 2 Collection 2)
            note: landsat level 2 collection 2 id = landsat_ot_c2_l2, landsat collection 1 level 1 id = lansat_8_c1
        latitude --> the latitude of the area the user wants the scene to cover
        longitude --> the longitude of the area the user wants the scene to cover
        start_date --> the start of the time range of data the user wants to obtain for a single area (string format: year-month-day)
        end_date --> the end of the time range of data the user wants to obtain for a single area (string format: year-month-day)
    Returns: The Landsat 8 scene selected from all the options by the user (downloaded to user's computer) that follows the conditions specified by the user in the function parameters, an LST map of the entire Landsat8 scene, an LST map of New York City specifically, and a zoomed-in LST map of the region specified by the user                             
    recommended dataset: LC08_L1TP_014032_20210826_20210901_01_T1
    '''
    colors = [(1, 1, 0), (1, 0, 0), (0.5, 0, 0.5)]  
    cmap = LinearSegmentedColormap.from_list('my_list', colors, N=100)
    colors2 = [(1, 1, 0), (0, 0, 1), (0, 1, 0)]  
    cmap2 = LinearSegmentedColormap.from_list('my_list', colors2, N=100)

    saved_data_exists = 0
    output_dir = './data'
    os.system('cls' if os.name == 'nt' else 'clear')
    try:
        with open('./data/dataset.txt', 'r') as file:
            landsat_product_id = file.read().rstrip()
        print(Fore.BLUE + "save data detected", Style.RESET_ALL)
        print("verify: ", Fore.BLUE + landsat_product_id, Style.RESET_ALL, " y/n")
        id = input()
        if(id == "n"):
            error
        try:
            output_filepath = output_dir + '/' + 'band_10_lst_Image.tiff'
            print(
                Fore.BLUE+"data detected in system. Using saved data.......", Style.RESET_ALL)
        except:
            print(Fore.RED+"unable to find data....", Style.RESET_ALL)
            saved_data_exists = 0
    except:
        print(Fore.RED + "not using save data", Style.RESET_ALL)
        id = "n"

    if(id == "n"):
        default_id = "LC08_L1TP_014032_20210826_20210901_01_T1"
        print("use default?: ", Fore.BLUE+default_id, Style.RESET_ALL, " y/n")
        default = input()
        print("enter username:")
        username = input()
        print("enter password:")
        password = pwinput()
        if (default == "y"):
            landsat_product_id = default_id
        else:
            # Initializing a new API instance and get an access key --> USGS account required
            try:
                api = API(username, password)
            # Searching for Landsat8 scenes (variables entered manually by user)
                scenes = api.search(
                    dataset=dataset,  # landsat level 2 collection 2 id = landsat_ot_c2_l2, landsat collection 1 level 1 id = lansat_8_c1 --> l2c2 contains surface temperature
                    latitude=latitude,  # lat and lon coordinates are those of nyc
                    longitude=longitude,
                    start_date=start_date,  # year-month-day
                    end_date=end_date,  # year-month-day
                    max_cloud_cover=0)
            except:
                print(
                    Fore.RED + "Unable to retrieve product IDs. Verify account", Style.RESET_ALL)
                return()
            print(f"{len(scenes)} scenes found.")

            # Processing the result
            for scene in scenes:
                print(scene['acquisition_date'])
                print(scene['landsat_product_id'])
            api.logout()

            # Specifying the single Landsat 8 Scene from all those found that will be downloaded
            print("Type in the Landsat Scene Product Identifier of the scene you wish to download (you can copy/paste this from the Landsat Product Identifiers generated above):")
            landsat_product_id = input()
        print("downloading data")
        # Downloading a single file from the scenes processed above
        try:
            ee = EarthExplorer(username, password)
            ee.download(landsat_product_id, output_dir='./data')
            ee.logout()
        except:
            print(
                Fore.RED + "Unable to retrieve product IDs. Verify account", Style.RESET_ALL)
            return()

        # Unzipping tar.gz file
        # output_dir='./data'
        tar_file = output_dir + '/' + landsat_product_id + '.tar.gz'
        file = tarfile.open(tar_file)
        # Extracting all Landsat8 files
        print("extrating data. please wait")
        file.extractall('./data')
        file.close()

    # convert the landsat_product_id into a string for the pathfiles
    os.listdir(output_dir)

    # Scrap metadata for variables needed
    variable_name_list = ['DATE_ACQUIRED', 'SCENE_CENTER_TIME', 'CORNER_UL_LAT_PRODUCT', 'CORNER_UL_LON_PRODUCT', 'CORNER_UR_LAT_PRODUCT', 'CORNER_UR_LON_PRODUCT', 'CORNER_LL_LAT_PRODUCT', 'CORNER_LL_LON_PRODUCT', 'CORNER_LR_LAT_PRODUCT', 'CORNER_LR_LON_PRODUCT', 'SUN_ELEVATION',
                          'RADIANCE_MULT_BAND_10', 'RADIANCE_MULT_BAND_11', 'RADIANCE_ADD_BAND_10',
                          'RADIANCE_ADD_BAND_11', 'REFLECTANCE_MULT_BAND_3', 'REFLECTANCE_MULT_BAND_4',
                          'REFLECTANCE_MULT_BAND_5', 'REFLECTANCE_MULT_BAND_6', 'REFLECTANCE_ADD_BAND_3',
                          'REFLECTANCE_ADD_BAND_4', 'REFLECTANCE_ADD_BAND_5', 'REFLECTANCE_ADD_BAND_6',
                          'K1_CONSTANT_BAND_10', 'K2_CONSTANT_BAND_10', 'K1_CONSTANT_BAND_11', 'K2_CONSTANT_BAND_11']

    band_4 = rasterio.open(
        output_dir + '/' + landsat_product_id + '_B4.TIF')  # red band
    # near infrared band (nir)
    band_5 = rasterio.open(
        output_dir + '/' + landsat_product_id + '_B5.TIF')
    band_10 = rasterio.open(
        output_dir + '/' + landsat_product_id + '_B10.TIF')  # thermal band
    metadata = (output_dir + '/' + landsat_product_id +
                '_MTL.txt')  # metadata file

    # Tidy up data
    scrap_lines = []
    with open(metadata, 'r') as list_file:
        for x in list_file:
            for variable in variable_name_list:
                if variable in x:
                    scrap_lines.append(x.rstrip('\n').strip())

    scrap_lines_clean = []
    for x in scrap_lines:
        scrap_lines_clean.append(x.split('=')[1].strip())

    # Tidy up dates
    DATE_ACQUIRED = scrap_lines_clean[0].replace('-', '')
    SCENE_CENTER_TIME1 = scrap_lines_clean[1][1:-1]
    SCENE_CENTER_TIME = SCENE_CENTER_TIME1.split('.')[0].replace(':', '')

    # Remove time variables
    noDates_variable_name_list = variable_name_list[2:]
    noDates_scraped_data = scrap_lines_clean[2:]
    floats_variable_list = [float(i) for i in noDates_scraped_data]

    # Create dictionary of variables
    variables_dict = dict(
        zip(noDates_variable_name_list, floats_variable_list))

    # Correct Sun Elevation
    sin_sun_elev = np.sin(np.deg2rad(variables_dict['SUN_ELEVATION']))
    corrected_sun_elev = float(sin_sun_elev)

    # Landsat 8 Band Editing
    red = band_4.read(1).astype('float64')
    nir = band_5.read(1).astype('float64')
    thermal = band_10.read(1).astype('float64')

    # Band 10 Radiance and Brightness Temperature
    band_10_radiance = ((variables_dict['RADIANCE_MULT_BAND_10'] *
                        thermal) + variables_dict['RADIANCE_ADD_BAND_10'])  # radiance
    band_10_surface_brightness_temp = ((variables_dict['K2_CONSTANT_BAND_10'] / np.log(
        (variables_dict['K1_CONSTANT_BAND_10'])/(band_10_radiance + 1))) - 273.15)  # brightness temperature

    # NDVI calculations
    red_f = ((variables_dict['REFLECTANCE_MULT_BAND_4'] * red) - variables_dict['REFLECTANCE_ADD_BAND_4']) / (
        corrected_sun_elev)  # red band with correct sun elevation
    nir_f = ((variables_dict['REFLECTANCE_MULT_BAND_5'] * nir) - variables_dict['REFLECTANCE_ADD_BAND_5'])/(
        corrected_sun_elev)  # nir band with correct sun elevation

    ndvi = np.where(
        (nir_f+red_f) == 0.,
        0,
        (nir_f-red_f)/(nir_f+red_f))

    # Calculate Proporional vegetation
    propveg = (((ndvi) - (0.2)) / (0.3))**2

    # Calculating LSE
    LSE = (0.004 * propveg) + 0.986

    # LST (land surface temperature) of band 10
    band_10_lst = (band_10_surface_brightness_temp / (1 + (10.895 *
                                                           (band_10_surface_brightness_temp/14380)) * (np.log(LSE))))

    # Plot LST using rasterio
    output_filepath = output_dir + '/' + 'band_10_lst_Image.tiff'
    band_10_lst_Image = rasterio.open(output_filepath, 'w', driver='Gtiff',
                                      width=band_10.width, height=band_10.height,
                                      count=1,
                                      crs=band_10.crs,
                                      transform=band_10.transform,
                                      dtype='float64')
    band_10_lst_Image.write(band_10_lst, 1)
    band_10_lst_Image.close()

    band_10_lst = rasterio.open(output_filepath)
    with open('./data/dataset.txt', 'w') as f:
        f.write(landsat_product_id)

    filepath = output_filepath


    # Format Image
    print("Plotting...")
    lst=plt.figure(1, figsize=(10, 10))
    x2 = max(variables_dict['CORNER_UL_LON_PRODUCT'], variables_dict['CORNER_UR_LON_PRODUCT'],
            variables_dict['CORNER_LL_LON_PRODUCT'], variables_dict['CORNER_LR_LON_PRODUCT'])
    print('x2 = ',x2)
    x1 = min(variables_dict['CORNER_UL_LON_PRODUCT'], variables_dict['CORNER_UR_LON_PRODUCT'],
            variables_dict['CORNER_LL_LON_PRODUCT'], variables_dict['CORNER_LR_LON_PRODUCT'])
    print('x1 = ',x1)
    y2 = max(variables_dict['CORNER_UL_LAT_PRODUCT'], variables_dict['CORNER_UR_LAT_PRODUCT'],
            variables_dict['CORNER_LL_LAT_PRODUCT'], variables_dict['CORNER_LR_LAT_PRODUCT'])
    print('y2 = ',y2)
    y1 = min(variables_dict['CORNER_UL_LAT_PRODUCT'], variables_dict['CORNER_UR_LAT_PRODUCT'],
            variables_dict['CORNER_LL_LAT_PRODUCT'], variables_dict['CORNER_LR_LAT_PRODUCT'])
    print('y1 = ',y1)

    
    window = rasterio.windows.Window(0, 0, band_10.width, band_10.height)
    with rasterio.open(filepath) as src:
        subset = src.read(1, window=window)
    plt.imshow(subset, cmap=cmap, extent=[x1,x2,y1,y2],vmin = 32.5, vmax = 50)
    plt.xlabel('x-axis coordinates')
    plt.ylabel('y-axis coordinates')
    plt.xlim(x1,x2)
    plt.ylim(y1,y2)
    plt.axis([longitude-size/2, longitude+size/2, latitude-size/2, latitude+size/2])
    plt.colorbar(shrink=0.5).ax.set_ylabel('Land Surface Temperature (°C)', rotation=90)
    plt.title(
        f'Land Surface Temperature Graph of Landsat 8 Scene 013032 Subset\n{window}')
    # plt.clim(32.5, 50)                                                                              # set colorbar range 
    plt.savefig("lst-"+landsat_product_id+".png")
    # plt.show()

    output_filepath = output_dir + '/' + 'ndviImage.tiff'

    ndviImage = rasterio.open(output_filepath, 'w', driver='Gtiff',
                            width=band_4.width, height=band_4.height,
                            count=1,
                            crs=band_4.crs,
                            transform=band_4.transform,
                            dtype='float64')
    ndviImage.write(ndvi, 1)
    ndviImage.close()

    ndvi = rasterio.open(output_filepath)
    # plot.show(ndvi)
    with rasterio.open(output_filepath) as src:
        subset = src.read(1, window=window)

    new_subset = subset
    nvdi=plt.figure(figsize=(10, 10))
    plt.imshow(subset, cmap='YlGn', extent=[x1,x2,y1,y2], vmin = 0, vmax = 0.45)
    plt.title(f'Normalized Difference Vegetation Index \n{landsat_product_id}')
    plt.xlabel('x-axis coordinates')
    plt.ylabel('y-axis coordinates')
    plt.xlim(x1,x2)
    plt.ylim(y1,y2)
    plt.axis([longitude-size/2, longitude+size/2, latitude-size/2, latitude+size/2])
    plt.colorbar(shrink=0.5).ax.set_ylabel('Wavelength?', rotation=90)
    # plt.clim(0, 0.45)                                                                              # set colorbar range 
    plt.savefig("nvdi-"+landsat_product_id+".png")
    return


def get_zoom_bounds(longitude, latitude, CORNER_UL_LAT_PRODUCT, CORNER_UL_LON_PRODUCT, CORNER_UR_LAT_PRODUCT, CORNER_UR_LON_PRODUCT, CORNER_LL_LAT_PRODUCT, CORNER_LL_LON_PRODUCT, CORNER_LR_LAT_PRODUCT, CORNER_LR_LON_PRODUCT, width, height, size):
    y2 = max(CORNER_UL_LON_PRODUCT, CORNER_UR_LON_PRODUCT,
             CORNER_LL_LON_PRODUCT, CORNER_LR_LON_PRODUCT)
    print('y2 = ',y2)
    y1 = min(CORNER_UL_LON_PRODUCT, CORNER_UR_LON_PRODUCT,
             CORNER_LL_LON_PRODUCT, CORNER_LR_LON_PRODUCT)
    print('y1 = ',y1)
    x2 = max(CORNER_UL_LAT_PRODUCT, CORNER_UR_LAT_PRODUCT,
             CORNER_LL_LAT_PRODUCT, CORNER_LR_LAT_PRODUCT)
    print('x2 = ',x2)
    x1 = min(CORNER_UL_LAT_PRODUCT, CORNER_UR_LAT_PRODUCT,
             CORNER_LL_LAT_PRODUCT, CORNER_LR_LAT_PRODUCT)
    print('x1 = ',x1)
    if(longitude < y1 or longitude > y2):
        print(Fore.RED + "Specified longitude is outside dataset range",
              Style.RESET_ALL)
    if(latitude < x1 or latitude > x2):
        print(Fore.RED + "Specified latitude is outside dataset range", Style.RESET_ALL)
    y = mapping(longitude, y1, y2, height, 'y')-size/2
    x = mapping(latitude, x1, x2, width, 'x')-size/2
    if(y < 0 or y > height):
        print(Fore.RED + "Specified longitude is outside dataset range",
              Style.RESET_ALL)
    if(x < 0 or x > width):
        print(Fore.RED + "Specified latitude is outside dataset range", Style.RESET_ALL)
    print(y1, " , ", longitude, " , ", x, " , ", y)
    return y, x


def mapping(value, p1, p2, width, type):
    pspan = p2-p1
    if(type == 'y'):
        scale = float(p2-value)/pspan
        # print('height: ', width)
    if(type == 'x'):
        scale = float(value-p1)/pspan
        # print('width: ', width)
    return scale*width


urban_greenery_satellite('landsat_8_c1', 40.813169, -
                         73.855537, '2021-07-01', '2021-09-01', 6400, 1900, 0.1)
