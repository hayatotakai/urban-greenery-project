#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 29 16:44:53 2021
Adapted on Thu Jun 28 24:09:09 2022
@author: meeramavroidis and hayatotakai
"""

import json
from tkinter.tix import Y_REGION
from landsatxplore.api import API
from landsatxplore.earthexplorer import EarthExplorer
import rasterio
from rasterio import plot
import numpy as np
import matplotlib.pyplot as plt
# %matplotlib inline
import matplotlib.colors as mcol
import matplotlib.cm as cm
import os
import tarfile
from colorama import Fore, Back, Style
from pwinput import pwinput
from matplotlib.colors import LinearSegmentedColormap
import shutil

#####################


def urban_greenery_satellite(dataset, latitude, longitude, start_date, end_date, size):
    '''
    urban_greenery_satellite: searches for, downloads, and produces images for a range of user-selected Landsat 8 data scene from the USGS archive (user must have a UGSG account) of a target coordinate, it then converts this data into a Land Surface Temperature (LST) and a NVDI map

    Arguments:
        dataset --> the specific Landsat 8 dataset format in string form (e.g. Landsat 8 Level 1 Collection 1, Landsat 8 Level 1 Collection 2, Landsat 8 Level 2 Collection 1, Landsat 8 Level 2 Collection 2)
        note: landsat level 2 collection 2 id = landsat_ot_c2_l2, landsat collection 1 level 1 id = lansat_8_c1
        latitude --> the target latitude for the center of the image
        longitude --> the target longitude for the center of the image
        start_date --> the start of the time range of data the user wants to obtain for a single area (string format: year-month-day)
        end_date --> the end of the time range of data the user wants to obtain for a single area (string format: year-month-day)
        size --> how zoomed in the image should be

    Returns: All Landsat 8 scenes availible within the selected options by the user (downloaded to user's computer) that follows the conditions specified by the user in the function parameters, an LST map and NVDI map
    '''
    output_dir='./data'
    colors = [(1, 1, 0), (1, 0, 0), (0.5, 0, 0.5)]
    cmap = LinearSegmentedColormap.from_list('my_list', colors, N=100)
    colors2 = [(1, 1, 0), (0, 0, 1), (0, 1, 0)]
    cmap2 = LinearSegmentedColormap.from_list('my_list', colors2, N=100)

    print("enter username:")
    username = input()
    print("enter password:")
    password = pwinput()
    # Initializing a new API instance and get an access key --> USGS account required
    try:
        api = API(username, password)
        # Searching for Landsat8 scenes (variables entered manually by user)
        scenes = api.search(
            dataset=dataset,  # landsat level 2 collection 2 id = landsat_ot_c2_l2, landsat collection 1 level 1 id = lansat_8_c1 --> l2c2 contains surface temperature
            latitude=latitude,  # lat and lon coordinates are those of nyc
            longitude=longitude,
            start_date=start_date,  # year-month-day
            end_date=end_date,  # year-month-day
            max_cloud_cover=10)
    except:
        print(Fore.RED + "Unable to retrieve product IDs. Verify account", Style.RESET_ALL)
        return()
    print(f"{len(scenes)} scenes found. Downloading all")
    api.logout()
    for scene in scenes:
        landsat_product_id = scene['landsat_product_id']
        print('acquisition_date: ', Fore.GREEN, scene['acquisition_date'], Style.RESET_ALL)
        print('landsat_product_id: ', Fore.BLUE, landsat_product_id, Style.RESET_ALL)
        download_incomplete = True
        while download_incomplete:
            try:
                ee = EarthExplorer(username, password)
                ee.download(landsat_product_id, output_dir='./data', timeout=30)
                ee.logout()
                download_incomplete = False
            except:
                download_incomplete = True
                ee.logout()
        # Unzipping tar.gz file
        tar_file = output_dir + '/' + landsat_product_id + '.tar.gz'
        file = tarfile.open(tar_file)
        # Extracting all Landsat8 files
        print("extrating data. please wait")
        file.extractall('./data')
        file.close()
        # convert the landsat_product_id into a string for the pathfiles
        os.listdir(output_dir)

        # Scrap metadata for variables needed
        variable_name_list = ['DATE_ACQUIRED', 'SCENE_CENTER_TIME', 'CORNER_UL_LAT_PRODUCT', 'CORNER_UL_LON_PRODUCT', 'CORNER_UR_LAT_PRODUCT', 'CORNER_UR_LON_PRODUCT', 'CORNER_LL_LAT_PRODUCT', 'CORNER_LL_LON_PRODUCT', 'CORNER_LR_LAT_PRODUCT', 'CORNER_LR_LON_PRODUCT', 'SUN_ELEVATION',
                                'RADIANCE_MULT_BAND_10', 'RADIANCE_MULT_BAND_11', 'RADIANCE_ADD_BAND_10',
                                'RADIANCE_ADD_BAND_11', 'REFLECTANCE_MULT_BAND_3', 'REFLECTANCE_MULT_BAND_4',
                                'REFLECTANCE_MULT_BAND_5', 'REFLECTANCE_MULT_BAND_6', 'REFLECTANCE_ADD_BAND_3',
                                'REFLECTANCE_ADD_BAND_4', 'REFLECTANCE_ADD_BAND_5', 'REFLECTANCE_ADD_BAND_6',
                                'K1_CONSTANT_BAND_10', 'K2_CONSTANT_BAND_10', 'K1_CONSTANT_BAND_11', 'K2_CONSTANT_BAND_11']

        band_4 = rasterio.open(output_dir + '/' + landsat_product_id + '_B4.TIF')  # red band
        # near infrared band (nir)
        band_5 = rasterio.open(output_dir + '/' + landsat_product_id + '_B5.TIF')
        band_10 = rasterio.open(output_dir + '/' + landsat_product_id + '_B10.TIF')  # thermal band
        metadata = (output_dir + '/' + landsat_product_id + '_MTL.txt')  # metadata file

        # Tidy up data
        scrap_lines = []
        with open(metadata, 'r') as list_file:
            for x in list_file:
                for variable in variable_name_list:
                    if variable in x:
                        scrap_lines.append(x.rstrip('\n').strip())

        scrap_lines_clean = []
        for x in scrap_lines: scrap_lines_clean.append(x.split('=')[1].strip())

        # Tidy up dates
        DATE_ACQUIRED = scrap_lines_clean[0].replace('-', '')
        SCENE_CENTER_TIME1 = scrap_lines_clean[1][1:-1]
        SCENE_CENTER_TIME = SCENE_CENTER_TIME1.split('.')[0].replace(':', '')

        # Remove time variables
        noDates_variable_name_list = variable_name_list[2:]
        noDates_scraped_data = scrap_lines_clean[2:]
        floats_variable_list = [float(i) for i in noDates_scraped_data]

        # Create dictionary of variables
        variables_dict = dict(zip(noDates_variable_name_list, floats_variable_list))

        # Correct Sun Elevation
        sin_sun_elev = np.sin(np.deg2rad(variables_dict['SUN_ELEVATION']))
        corrected_sun_elev = float(sin_sun_elev)

        # Landsat 8 Band Editing
        red = band_4.read(1).astype('float64')
        nir = band_5.read(1).astype('float64')
        thermal = band_10.read(1).astype('float64')

        # Band 10 Radiance and Brightness Temperature
        band_10_radiance = ((variables_dict['RADIANCE_MULT_BAND_10'] * thermal) + variables_dict['RADIANCE_ADD_BAND_10'])  # radiance
        band_10_surface_brightness_temp = ((variables_dict['K2_CONSTANT_BAND_10'] / np.log((variables_dict['K1_CONSTANT_BAND_10'])/(band_10_radiance + 1))) - 273.15)  # brightness temperature

        # NDVI calculations
        red_f = ((variables_dict['REFLECTANCE_MULT_BAND_4'] * red) - variables_dict['REFLECTANCE_ADD_BAND_4']) / (corrected_sun_elev)  # red band with correct sun elevation
        nir_f = ((variables_dict['REFLECTANCE_MULT_BAND_5'] * nir) - variables_dict['REFLECTANCE_ADD_BAND_5'])/(corrected_sun_elev)  # nir band with correct sun elevation

        ndvi = np.where((nir_f+red_f) == 0., 0, (nir_f-red_f)/(nir_f+red_f))

        # Calculate Proporional vegetation
        propveg = (((ndvi) - (0.2)) / (0.3))**2

        # Calculating LSE
        LSE = (0.004 * propveg) + 0.986

        # LST (land surface temperature) of band 10
        band_10_lst = (band_10_surface_brightness_temp / (1 + (10.895 * (band_10_surface_brightness_temp/14380)) * (np.log(LSE))))

        # ----------Find Extents---------- #
        print("Plotting...")
        x2 = max(variables_dict['CORNER_UL_LON_PRODUCT'], variables_dict['CORNER_UR_LON_PRODUCT'],
                    variables_dict['CORNER_LL_LON_PRODUCT'], variables_dict['CORNER_LR_LON_PRODUCT'])
        print('x2 = ', x2)
        x1 = min(variables_dict['CORNER_UL_LON_PRODUCT'], variables_dict['CORNER_UR_LON_PRODUCT'],
                    variables_dict['CORNER_LL_LON_PRODUCT'], variables_dict['CORNER_LR_LON_PRODUCT'])
        print('x1 = ', x1)
        y2 = max(variables_dict['CORNER_UL_LAT_PRODUCT'], variables_dict['CORNER_UR_LAT_PRODUCT'],
                    variables_dict['CORNER_LL_LAT_PRODUCT'], variables_dict['CORNER_LR_LAT_PRODUCT'])
        print('y2 = ', y2)
        y1 = min(variables_dict['CORNER_UL_LAT_PRODUCT'], variables_dict['CORNER_UR_LAT_PRODUCT'],
                    variables_dict['CORNER_LL_LAT_PRODUCT'], variables_dict['CORNER_LR_LAT_PRODUCT'])
        print('y1 = ', y1)
        window = rasterio.windows.Window(0, 0, band_10.width, band_10.height)
        
        # ----------Plot LST---------- #
        output_filepath = output_dir + '/' + 'band_10_lst_Image.tiff'
        band_10_lst_Image = rasterio.open(output_filepath, 'w', driver='Gtiff',
                                            width=band_10.width, height=band_10.height,
                                            count=1,
                                            crs=band_10.crs,
                                            transform=band_10.transform,
                                            dtype='float64')
        band_10_lst_Image.write(band_10_lst, 1)
        band_10_lst_Image.close()
        band_10_lst = rasterio.open(output_filepath)
        with open('./data/dataset.txt', 'w') as f:f.write(landsat_product_id)
        filepath = output_filepath
        lst = plt.figure(figsize=(10, 10))
        with rasterio.open(filepath) as src:subset = src.read(1, window=window)
        plt.imshow(subset, cmap=cmap, extent=[x1, x2, y1, y2])
        plt.xlabel('Longitude')
        plt.ylabel('Latitude')
        plt.xlim(x1, x2)
        plt.ylim(y1, y2)
        plt.title(f'LST Graph of Landsat 8 Scene: \n{landsat_product_id}')
        plt.axis([longitude-size/2, longitude+size /2, latitude-size/2, latitude+size/2])               # zoom to bounds 
        plt.colorbar(shrink=0.5).ax.set_ylabel('Land Surface Temperature (°C)', rotation=90)
        plt.clim(32, 45)                                                                              # set colorbar range 
        plt.savefig("lst-" + landsat_product_id + '.png')                                               # save fig to home directory
        # plt.show()                                                                                    # uncomment to show plot each time

        # ----------Plot NVDI---------- #
        output_filepath = output_dir + '/' + 'ndviImage.tiff'
        ndviImage = rasterio.open(output_filepath, 'w', driver='Gtiff',
                                    width=band_4.width, height=band_4.height,
                                    count=1,
                                    crs=band_4.crs,
                                    transform=band_4.transform,
                                    dtype='float64')
        ndviImage.write(ndvi, 1)
        ndviImage.close()
        ndvi = rasterio.open(output_filepath)
        with rasterio.open(output_filepath) as src:subset = src.read(1, window=window)
        new_subset = subset
        nvdi = plt.figure(figsize=(10, 10))
        plt.imshow(subset, cmap='YlGn', extent=[x1, x2, y1, y2])
        plt.title(f'NDVI Graph of Landsat 8 Scene: \n{landsat_product_id}')
        plt.xlabel('Longitude')
        plt.ylabel('Latitude')
        plt.xlim(x1, x2)
        plt.ylim(y1, y2)
        plt.axis([longitude-size/2, longitude+size /2, latitude-size/2, latitude+size/2])               # zoom to bounds 
        plt.colorbar(shrink=0.5).ax.set_ylabel('NVDI', rotation=90)
        plt.clim(-0.1, 0.45)                                                                               # set colorbar range 
        plt.savefig("nvdi-" + landsat_product_id + '.png')                                              # save fig to home directory
        # plt.show()                                                                                    # uncomment to show plot each time
        shutil.rmtree(output_dir)                                                                                
    return

urban_greenery_satellite('landsat_8_c1', 40.813169, -73.885537, '2021-06-01', '2021-09-15', 0.1)