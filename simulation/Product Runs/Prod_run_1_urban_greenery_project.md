##  Setup of production runs for Urban Greenery Canopy Flow Simulations
  
  
Numerical setups of production runs for Urban Greenery flow simulations are presented in this page. The goal of this simulation is to show the effects of forest width and height on the ambient temperature. We consider a radially symmetric geometry with the forest at the center and an evenly spaced radial array of cuboids. The radially symmetric nature of the geometry allows us to observe a characteristic plane. The geometry is simplified to an open channel flow of air over an array of evenly spaced square buildings and a rectangular forest as shown in Fig. 1 below. 
![Diagram 2 of Urban Greenery Project Simulation](canopy%20flow%20version%202.png )*Fig.1 Diagram of the computational domain*
  
A series of standard k-omega simulations are conducted where the size of the computational domain is <img src="https://latex.codecogs.com/gif.latex?L_z%20&#x5C;times%20L_x%20=%20(10h)+(L_i+L_p+L_o)"/>, where building height <img src="https://latex.codecogs.com/gif.latex?(h=10m)"/>, so <img src="https://latex.codecogs.com/gif.latex?L_z=100m"/>, where the buildings by the inlet <img src="https://latex.codecogs.com/gif.latex?(L_i=1000m)"/>, and the buildings by the outlet <img src="https://latex.codecogs.com/gif.latex?(L_o=500m)"/>. The length of the park <img src="https://latex.codecogs.com/gif.latex?(L_P)"/>, the height of the park <img src="https://latex.codecogs.com/gif.latex?(H_P)"/>, and the frontal area <img src="https://latex.codecogs.com/gif.latex?(&#x5C;lambda)"/> are varied. The value of <img src="https://latex.codecogs.com/gif.latex?L_i"/> was chosen to be sufficiently long for the uniform inlet velocity to achieve a fully developed state.  <img src="https://latex.codecogs.com/gif.latex?&#x5C;lambda"/> is directly proportional to <img src="https://latex.codecogs.com/gif.latex?L_P"/>, representing short dense forests <img src="https://latex.codecogs.com/gif.latex?(H_P=5m:&#x5C;lambda=0.1m^2&#x2F;m^3)"/> and tall sparse forests <img src="https://latex.codecogs.com/gif.latex?(H_P=20m:&#x5C;lambda=0.025m^2&#x2F;m^3)"/>. The setup will effectively contrast the capabilities of each configuration. The physical parameters of each product run are summarized in Table 1. below. 
  
|#|<img src="https://latex.codecogs.com/gif.latex?&#x5C;lambda%20(m^2&#x2F;m^3)"/>|<img src="https://latex.codecogs.com/gif.latex?L_P%20(m)"/>|<img src="https://latex.codecogs.com/gif.latex?H_P%20(m)"/>|
|-|-|-|-|
|L10_D0p4_H0p05|0.1|40|5|
|L2p5_D0p4_H0p2|0.025|40|20|
|L10_D0p8_H0p05|0.1|80|5|
|L2p5_D0p8_H0p2|0.025|80|20|
  
*Table 1. Summary of CFD runs*
  
The computational domain is designated a gravitational acceleration of <img src="https://latex.codecogs.com/gif.latex?-9.81m&#x2F;s^2"/> in the z-direction. In addition, a free-slip boundary condition to the top vertex, a uniform velocity of <img src="https://latex.codecogs.com/gif.latex?30&#x5C;degree%20C"/> at <img src="https://latex.codecogs.com/gif.latex?1&#x5C;text{m&#x2F;s}"/> to the inlet <img src="https://latex.codecogs.com/gif.latex?(U_&#x5C;infty)"/>, and an outlet preventing backflow are initialized. The atmospheric zone fluid is air, initialized with a density that is modeled as an ideal gas. The initialized properties of air are summarized in Table 2. below. 
  
|Property|Value|
|-|-|
|Density <img src="https://latex.codecogs.com/gif.latex?(&#x5C;rho_{&#x5C;text{air}})"/>|ideal-gas|
|Specific heat <img src="https://latex.codecogs.com/gif.latex?(&#x5C;text{Cp}_{&#x5C;text{air}})"/>|<img src="https://latex.codecogs.com/gif.latex?1006.43&#x5C;space%20&#x5C;text{J&#x2F;kg}%20&#x5C;space%20&#x5C;text{K}"/>|
|Thermal conductivity <img src="https://latex.codecogs.com/gif.latex?(k_{&#x5C;text{air}})"/>|<img src="https://latex.codecogs.com/gif.latex?0.0242&#x5C;space%20&#x5C;text{W&#x2F;}(&#x5C;text{m}&#x5C;space%20&#x5C;text{K})"/>|
|Viscosity <img src="https://latex.codecogs.com/gif.latex?(&#x5C;nu_{&#x5C;text{air}})"/>|<img src="https://latex.codecogs.com/gif.latex?1.7894&#x5C;times10^{-5}%20&#x5C;space&#x5C;text{kg&#x2F;}(&#x5C;text{m}&#x5C;space%20&#x5C;text{s})"/>|
|Molecular weight <img src="https://latex.codecogs.com/gif.latex?(M_{r_{&#x5C;text{air}}})"/>|<img src="https://latex.codecogs.com/gif.latex?28.966&#x5C;space%20&#x5C;text{kg}&#x2F;&#x5C;text{kmol}"/>|
  
*Table 2. Summary of Air Properties*
  
The buildings have a height, width, and spacing of <img src="https://latex.codecogs.com/gif.latex?h=10m"/>. The buildings and the ground are made of `concrete` at a temperature of <img src="https://latex.codecogs.com/gif.latex?T_i=35&#x5C;degree%20C"/>. The properties of concrete are summarized in Table 3. below.
  
|Property|Value|
|-|-|
|Density <img src="https://latex.codecogs.com/gif.latex?(&#x5C;rho_{&#x5C;text{concrete}})"/>|<img src="https://latex.codecogs.com/gif.latex?2391.7%20&#x5C;space%20&#x5C;text{kg}&#x2F;&#x5C;text{m}^3"/>|
|Specific heat <img src="https://latex.codecogs.com/gif.latex?(&#x5C;text{Cp}_{&#x5C;text{concrete}})"/>|<img src="https://latex.codecogs.com/gif.latex?936.35&#x5C;space%20&#x5C;text{J&#x2F;kg}%20&#x5C;space%20&#x5C;text{K}"/>|
|Thermal conductivity <img src="https://latex.codecogs.com/gif.latex?(k_{&#x5C;text{concrete}})"/>|<img src="https://latex.codecogs.com/gif.latex?2.0712&#x5C;space%20&#x5C;text{W&#x2F;}(&#x5C;text{m}&#x5C;space%20&#x5C;text{K})"/>|
|Sand-grain roughness <img src="https://latex.codecogs.com/gif.latex?(R_{&#x5C;text{concrete}})"/>|<img src="https://latex.codecogs.com/gif.latex?1.5&#x5C;text{%20mm}"/>|
  
*Table 3. Summary of Concrete Properties*  
  
The forest will have a temperature of <img src="https://latex.codecogs.com/gif.latex?T_f=25&#x5C;degree%20C"/> and a drag force of <img src="https://latex.codecogs.com/gif.latex?F_d=C_D&#x5C;hat{&#x5C;lambda}_F%20&#x5C;|%20&#x5C;hat{u}&#x5C;|%20&#x5C;hat{u}_i"/>, which arises from the navier stokes representation, <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{&#x5C;partial%20u_i}{&#x5C;partial%20t}+u_j&#x5C;frac{&#x5C;partial%20u_i}{&#x5C;partial%20x_j}=F_D-&#x5C;frac{1}{&#x5C;rho}&#x5C;frac{&#x5C;partial%20p}{&#x5C;partial%20x_i}+&#x5C;nu%20&#x5C;frac{&#x5C;partial%20^2%20u_i}{&#x5C;partial%20x_j%20&#x5C;partial%20x_j}"/>, where <img src="https://latex.codecogs.com/gif.latex?C_D=0.2"/>.
  
The maximum domain size in Ansys Fluent is <img src="https://latex.codecogs.com/gif.latex?L_x&#x5C;leq%201000"/> units. To work within this restriction, the model was non-dimensionalized by <img src="https://latex.codecogs.com/gif.latex?U_{&#x5C;infty}"/>, <img src="https://latex.codecogs.com/gif.latex?L_z"/>, and <img src="https://latex.codecogs.com/gif.latex?&#x5C;rho%20_{&#x5C;text{air}}"/> where <img src="https://latex.codecogs.com/gif.latex?&#x5C;text{Re}=1&#x2F;&#x5C;hat{&#x5C;nu}_{&#x5C;text{air}}=U_&#x5C;infty%20L_z&#x2F;&#x5C;nu%20_{&#x5C;text{air}}=5.588%20&#x5C;times%2010^8"/>, <img src="https://latex.codecogs.com/gif.latex?&#x5C;hat{R}_{&#x5C;text{concrete}}=R_{&#x5C;text{concrete}}&#x2F;L_z=0.015%20&#x5C;text{mm}"/>, and 
<img src="https://latex.codecogs.com/gif.latex?&#x5C;hat{&#x5C;lambda}%20=%20&#x5C;lambda%20L_z=%20&#x5C;begin{cases}%2010%20&#x5C;;%20&#x5C;text{if%20}%20&#x5C;hat{&#x5C;text{H}}_&#x5C;text{P}=0.05m&#x5C;&#x5C;2.5%20&#x5C;;%20&#x5C;text{if%20}%20&#x5C;hat{&#x5C;text{H}}_&#x5C;text{P}%20=%200.2m%20&#x5C;end{cases}"/>
  
The non-dimensionalized parameters are summarized in Table 4. below. 
  
|#|<img src="https://latex.codecogs.com/gif.latex?&#x5C;hat{&#x5C;lambda}"/>|<img src="https://latex.codecogs.com/gif.latex?&#x5C;hat{L}_P%20(m)"/>|<img src="https://latex.codecogs.com/gif.latex?&#x5C;hat{H}_P%20(m)"/>|
|-|-|-|-|
|L10_D0p4_H0p05|10|0.4|0.05|
|L2p5_D0p4_H0p2|2.5|0.4|0.2|
|L10_D0p8_H0p05|10|0.8|0.05|
|L2p5_D0p8_H0p2|2.5|0.8|0.2|
  
*Table 4. Summary of non-dimensionalized parameters*
  
The simulation will be run for 300 iterations to achieve steady state. 
  
###  Grid sensitivity analysis
  
###  Appendix
  
  
####  Why are these <img src="https://latex.codecogs.com/gif.latex?L_P"/> considered?
  
  
These <img src="https://latex.codecogs.com/gif.latex?L_P"/> are evaluated because they will be capable of showing how varying <img src="https://latex.codecogs.com/gif.latex?L_P"/> effects the ambient temperature. To put into scale, <img src="https://latex.codecogs.com/gif.latex?L_P%20=%2040"/> represents two building and street widths, and <img src="https://latex.codecogs.com/gif.latex?L_P%20=%2080"/> represents four building and street widths.
  
####  Why are these <img src="https://latex.codecogs.com/gif.latex?H_P"/> considered?
  
  
The assigned values for <img src="https://latex.codecogs.com/gif.latex?H_P"/> are evaluated because they correspond to half of <img src="https://latex.codecogs.com/gif.latex?h"/> when <img src="https://latex.codecogs.com/gif.latex?H_P=5%20m"/> and double of <img src="https://latex.codecogs.com/gif.latex?h"/> when <img src="https://latex.codecogs.com/gif.latex?H_P%20=%2020%20m"/>. These values for <img src="https://latex.codecogs.com/gif.latex?H_P"/> will influence the decision on the genus of foliage appropriate for the project by uncovering the optimal height of the forest. 
  
  