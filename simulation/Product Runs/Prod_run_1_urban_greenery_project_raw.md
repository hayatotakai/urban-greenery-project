## Setup of production runs for Urban Greenery Canopy Flow Simulations

Numerical setups of production runs for Urban Greenery flow simulations are presented in this page. The goal of this simulation is to show the effects of forest width and height on the ambient temperature. We consider a radially symmetric geometry with the forest at the center and an evenly spaced radial array of cuboids. The radially symmetric nature of the geometry allows us to observe a characteristic plane. The geometry is simplified to an open channel flow of air over an array of evenly spaced square buildings and a rectangular forest as shown in Fig. 1 below.
![Diagram 2 of Urban Greenery Project Simulation](canopy%20flow%20version%202.png)_Fig.1 Diagram of the computational domain_

A series of standard k-omega simulations are conducted where the size of the computational domain is $L_z\times L_x = (10h)\times(L_i+L_p+L_o)$, where building height $(h=10m)$, so $L_z=100m$, where the buildings by the inlet $(L_i=1000m)$, and the buildings by the outlet $(L_o=500m)$. The length of the park $(L_P)$, the height of the park $(H_P)$, and the frontal area $(\lambda)$ are varied. The value of $L_i$ was chosen to be sufficiently long for the uniform inlet velocity to achieve a fully developed state. $\lambda$ is directly proportional to $L_P$, representing short dense forests $(H_P=5m:\lambda=0.1m^2/m^3)$ and tall sparse forests $(H_P=20m:\lambda=0.025m^2/m^3)$. The setup will effectively contrast the capabilities of each configuration. The physical parameters of each product run are summarized in Table 1. below.

| #              | $\lambda (m^2/m^3)$ | $L_P (m)$ | $H_P (m)$ |
| -------------- | ------------------- | --------- | --------- |
| L10_D0p4_H0p05 | 0.1                 | 40        | 5         |
| L2p5_D0p4_H0p2 | 0.025               | 40        | 20        |
| L10_D0p8_H0p05 | 0.1                 | 80        | 5         |
| L2p5_D0p8_H0p2 | 0.025               | 80        | 20        |

_Table 1. Summary of CFD runs_

The computational domain is designated a gravitational acceleration of $-9.81m/s^2$ in the z-direction. In addition, a free-slip boundary condition to the top vertex, a uniform velocity of $30\degree C$ at $1\text{m/s}$ to the inlet $(U_\infty)$, and an outlet preventing backflow are initialized. The atmospheric zone fluid is air, initialized with a density that is modeled as an ideal gas. The initialized properties of air are summarized in Table 2. below.

| Property                                 | Value                                                           |
| ---------------------------------------- | --------------------------------------------------------------- |
| Density $(\rho_{\text{air}})$            | ideal-gas                                                       |
| Specific heat $(\text{Cp}_{\text{air}})$ | $1006.43\space \text{J/kg} \space \text{K}$                     |
| Thermal conductivity $(k_{\text{air}})$  | $0.0242\space \text{W/}(\text{m}\space \text{K})$               |
| Viscosity $(\nu_{\text{air}})$           | $1.7894\times10^{-5} \space\text{kg/}(\text{m}\space \text{s})$ |
| Molecular weight $(M_{r_{\text{air}}})$  | $28.966\space \text{kg}/\text{kmol}$                            |

_Table 2. Summary of Air Properties_

The buildings have a height, width, and spacing of $h=10m$. The buildings and the ground are made of `concrete` at a temperature of $T_i=35\degree C$. The properties of concrete are summarized in Table 3. below.

| Property                                      | Value                                             |
| --------------------------------------------- | ------------------------------------------------- |
| Density $(\rho_{\text{concrete}})$            | $2391.7 \space \text{kg}/\text{m}^3$              |
| Specific heat $(\text{Cp}_{\text{concrete}})$ | $936.35\space \text{J/kg} \space \text{K}$        |
| Thermal conductivity $(k_{\text{concrete}})$  | $2.0712\space \text{W/}(\text{m}\space \text{K})$ |
| Sand-grain roughness $(R_{\text{concrete}})$  | $1.5\text{ mm}$                                   |

_Table 3. Summary of Concrete Properties_

The forest will have a temperature of $T_f=25\degree C$ and a drag force of $F_{d_i}=-C_D\hat{\lambda}_F \| \hat{u}\| \hat{u}_i$, which arises from the navier stokes representation, $\frac{\partial u_i}{\partial t}+u_j\frac{\partial u_i}{\partial x_j}=F_{D_i}-\frac{1}{\rho}\frac{\partial p}{\partial x_i}+\nu \frac{\partial ^2 u_i}{\partial x_j \partial x_j}$, where $C_D=0.2$.

The maximum domain size in Ansys Fluent is $L_x\leq 1000$ units. To work within this restriction, the model was non-dimensionalized by $U_{\infty}$, $L_z$, and $\rho _{\text{air}}$ where $\text{Re}=1/\hat{\nu}_{\text{air}}=U_\infty L_z/\nu _{\text{air}}=5.588 \times 10^6$, $\hat{R}_{\text{concrete}}=R_{\text{concrete}}/L_z=0.015 \text{mm}$, and
$\hat{\lambda} = \lambda /L_z= \begin{cases} 10 \; \text{if } \hat{\text{H}}_\text{P}=0.05m\\
2.5\; \text{if } \hat{\text{H}}_\text{P} = 0.2m \end{cases}$

The non-dimensionalized parameters are summarized in Table 4. below.

| #              | $\hat{\lambda}$ | $\hat{L}_P (m)$ | $\hat{H}_P (m)$ |
| -------------- | --------------- | --------------- | --------------- |
| L10_D0p4_H0p05 | 10              | 0.4             | 0.05            |
| L2p5_D0p4_H0p2 | 2.5             | 0.4             | 0.2             |
| L10_D0p8_H0p05 | 10              | 0.8             | 0.05            |
| L2p5_D0p8_H0p2 | 2.5             | 0.8             | 0.2             |

_Table 4. Summary of non-dimensionalized parameters_

The simulation will be run for 300 iterations to achieve steady state.

### Grid sensitivity analysis

A grid sensitivity analysis was not conducted for this simulation, however it is assumed that a cartesian grid of $1m$ is sufficient for this application.

### Appendix

#### Why are these $L_P$ considered?

These $L_P$ are evaluated because they will be capable of showing how varying $L_P$ effects the ambient temperature. To put into scale, $L_P = 40$ represents two building and street widths, and $L_P = 80$ represents four building and street widths.

#### Why are these $H_P$ considered?

The assigned values for $H_P$ are evaluated because they correspond to half of $h$ when $H_P=5 m$ and double of $h$ when $H_P = 20 m $. These values for $H_P$ will influence the decision on the genus of foliage appropriate for the project by uncovering the optimal height of the forest.
