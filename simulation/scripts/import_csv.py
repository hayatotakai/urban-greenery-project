"""
Created on Thu Jul 21 3:37:00 2022
@author: hayatotakai
"""

import csv
import os
import string
import matplotlib.pyplot as plt
from statistics import mean

##########################################

def extractData(fileName, Li, Lp, Lo, h, shift):
    '''
    extractData: Removes building area data from csv plot created from ANSYS Fluent and Plots the data
    Arguments:
        fileName --> Path to csv file
        Li --> Distance of buildings preceding park
        Lp --> Length of park
        Lo --> Distance of buildings secceding park
        h --> width of building and street
        shift --> distance to shift plot
    Returns: Plot of temperature 
    '''
    os.system('cls' if os.name == 'nt' else 'clear')
    file = open(fileName, "r")
    dataStart = 5                                                               # removes precedding nondata lines
    data = list(csv.reader(file, delimiter=","))[dataStart:]
    file.close()

    iprev, x, temp, numdata, streetdata, xavg = -999, [], [], [], [], [0]
    for i in data:
        try:
            if i[0] != 'undef' and float(i[0]) > iprev:                         # remove 'undef' from list
                numdata.append(i)                                               # rebuild list
                iprev = float(i[0])                                             # force list order
        except:
            break
    odd_list = [-18 + x / 10 for x in range(round((Li+3*h)/h)) if x % 2 == 0] + [-18+Li+2*h+x / 10 for x in range(round((Lp+Lo)/h)) if x % 2 != 0]
    odd_index , park_offset = 0, 0
    for j in numdata:
        try:
            if float(j[0]) >= odd_list[odd_index]+h:
                x.append(odd_list[odd_index])                                   # get distance for street width
                try:
                    temp.append(mean(xavg))                                     # get average temperature for street width
                except:
                    temp.append(0)
                xavg = []
                odd_index = odd_index+1
            if float(j[0]) >= odd_list[odd_index] and float(j[1]) <= 34:
                xavg.append(float(j[1]))                                        # temporary list of street width values to be averaged
        except:
            break
    for z in range(len(x)) : x[z] = (x[z] + shift + 18)*100                     # align plots
    plt.plot(x, temp, label=fileName.replace('./simulation/data/Temperature for ', ''))
    return

##########################################

extractData('./simulation/data/Temperature for L2p5_D0p4_H0p2.csv', 10, 0.4, 5, 0.1, 0)
extractData('./simulation/data/Temperature for L10_D0p4_H0p05.csv', 10, 0.4, 5, 0.1, 0)
extractData('./simulation/data/Temperature for L2p5_D0p8_H0p2.csv', 10, 0.8, 5, 0.1, -0.4)
extractData('./simulation/data/Temperature for L10_D0p8_H0p05.csv', 10, 0.8, 5, 0.1, -0.4)

plt.ylabel('Temperature [C]')
plt.xlabel('Distance [m]')
plt.title('Simulation Temperature Variations with Park Dimensions')
plt.legend()
plt.xlim(0, 1800)
plt.ylim(30, 35)
plt.axvspan(1080, 1800, color='blue', alpha=0.1)
plt.show()